angular.module("spbTestApp").config (
		$routeProvider,
		$locationProvider
) ->

#    $locationProvider.html5Mode(true) if window.history && window.history.pushState
	$locationProvider.html5Mode(false)

	$routeProvider
		.when("/index",
			templateUrl: "views/index/index.html"
			controller: "IndexController"
		)
		.otherwise redirectTo: "/index"