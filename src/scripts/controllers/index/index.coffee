angular.module("spbTestApp").controller "IndexController", (
	$scope,
	IndexStore
) ->

	$scope.selectedItems = []
	$scope.tmpSelectedItems = []
	$scope.items = []
	$scope.ready = false
	$scope.dataLength = 0

	profile = (f, async) ->
		t1 = (new Date()).getTime()

		end = ()->
			t2 = (new Date()).getTime()
			console.log (t2-t1)/1000, "sec"

		f(end)

		unless async
			end()

	IndexStore.loadItems().then (dataLength)->
		$scope.items = IndexStore.getItems()
		$scope.dataLength = dataLength
		$scope.ready = true

	updateView = ()->
		$scope.items = IndexStore.getItems()
		$scope.selectedItems = IndexStore.getSelectedItems()
		$scope.tmpSelectedItems = []

	$scope.clearSelection = () ->
		$scope.tmpSelectedItems = []

	$scope.isSelected = (item) ->
		return item in $scope.tmpSelectedItems

	$scope.select = (e, item) ->
		unless e.ctrlKey
			$scope.tmpSelectedItems = []

		if $scope.isSelected item
			$scope.tmpSelectedItems.splice $scope.tmpSelectedItems.indexOf(item), 1
		else
			$scope.tmpSelectedItems.push item

	$scope.moveItems = (items) ->
		IndexStore.moveItems items.slice 0
		updateView()

	$scope.moveItemsBack = (items) ->
		IndexStore.moveItemsBack items.slice 0
		updateView()



