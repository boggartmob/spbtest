angular.module("spbTestApp").factory "IndexStore", (
	$q,
	$http,
	$timeout
) ->

	state =
		itemsCache: []
		items: []
		selectedItems: []

	self = {}

	self.getItems = () ->
		state.items = _.difference state.itemsCache, state.selectedItems
		return state.items

	self.getSelectedItems = () ->
		state.selectedItems = _.difference state.itemsCache, state.items
		return state.selectedItems

	self.moveItems = (items) ->
		for item in items
			self.moveItem item, state.items, state.selectedItems

	self.moveItemsBack = (items) ->
		for item in items
			self.moveItem item, state.selectedItems, state.items

	self.moveItem = (item, from, to) ->
		index = from.indexOf item
		if index >= 0
			to.push (from.splice index, 1)[0]

	self.loadItems = ()->
		deferred = $q.defer();

		$http.get("/items.json").success (data)->
			state.itemsCache = data
			state.items = data
			$timeout ()->
				deferred.resolve(data.length)
			, 500
		.error (e)->
			deferred.reject("Error message")

		return deferred.promise

	return self