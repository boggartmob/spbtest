# README #

## Environment ##

You need grunt and hs packages globaly installed


```
#!bash

npm -g install hs
npm -g install grunt
```

## Building ##

Go to the project dir and run 

```
#!bash

npm i
bower install
grunt
```

## Launch ##
Go to the project dir and run 
```
#!bash

hs ./dist
```

Have fun.